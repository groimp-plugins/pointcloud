package de.grogra.pointcloud;

import java.io.File;
import java.util.ArrayList;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
//import de.grogra.icon.IconSource;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.MimeTypeItem;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ProgressMonitor;
import de.grogra.pf.io.ProjectLoader;
import de.grogra.pf.io.ResourceLoader;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemVisitor;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.RegistryContext;
import de.grogra.pf.registry.TypeItem;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.Showable;
import de.grogra.pf.ui.TextEditor;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.PanelFactory;
import de.grogra.pf.ui.util.LockProtectedCommand;
import de.grogra.reflect.Method;
import de.grogra.reflect.Type;
//import de.grogra.rgg.RGGRoot;
import de.grogra.util.Lock;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;
import de.grogra.util.Utils;
import de.grogra.vfs.FileSystem;
import de.grogra.vfs.LocalFileSystem;
import de.grogra.xl.lang.ObjectToBoolean;

public class PointCloudSourceFile extends Item implements
		de.grogra.pf.ui.registry.UIItem, ObjectToBoolean,
		TreeModelListener, Showable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static class Loader extends FilterBase implements ObjectSource,
			ProjectLoader
	{
		public Loader (FilterItem item, FilterSource source)
		{
			super (item, source);
			setFlavor (IOFlavor.PROJECT_LOADER);
		}

		public Object getObject ()
		{
			return this;
		}
		public void loadRegistry (Registry r)
		{
			File f = ((FileSource) source).getInputFile ();
			Workbench w = Workbench.get (r);
			r.initFileSystem (new LocalFileSystem (IO.PROJECT_FS, f.getParentFile ()));
			if (w != null)
			{
				Item i = item.getItem ("layout");
				if (i != null)
				{
					i = i.resolveLink (r);
				}
				if (i != null)
				{
					w.setProperty (Workbench.INITIAL_LAYOUT, i.getAbsoluteName ());
				//	sf.showLater (w);
				}
			}		
		}

		
	
		
		public void loadGraph (Registry r)
		{
			File f = ((FileSource) source).getInputFile ();
			
		
			de.grogra.imp3d.objects.PointCloud[] array;
			array = PointCloudTools.importFromFile(f.getAbsolutePath());
			r.setEmptyGraph();
			GraphManager graph = r.getProjectGraph();
			graph.setRoot("test",new Node());
			Node root = graph.getRoot();
		
			for(de.grogra.imp3d.objects.PointCloud a : array) {
				root.addEdgeBitsTo(a,Graph.BRANCH_EDGE, null);
			}

		}
	}

	protected MimeType mimeType;
	//enh:field

	protected boolean editable = true;
	//enh:field getter
	
	protected boolean disabled = false;
	//enh:field getter setter

	protected transient String deactivationCategory;
	protected transient int activationStamp = -1;
	
	public int getActivationStamp()
	{
		return activationStamp;
	}
	
	public void setActivationStamp(int stamp)
	{
		this.activationStamp = stamp;
	}
	
	private PointCloudSourceFile ()
	{
		this (null, null);
	}

	public PointCloudSourceFile (String key, MimeType mimeType)
	{
		super (key);
		this.mimeType = mimeType;
	}

	public FileSource toFileSource ()
	{
		return FileSource.createFileSource (getName (), mimeType, this, new StringMap (this));
	}
	
	public MimeType getMimeType ()
	{
		return mimeType;
	}
	
	public MimeTypeItem getMimeTypeItem ()
	{
		return MimeTypeItem.get (this, mimeType);
	}
	
	
	
	@Override
	public void show(Context ctx) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void treeNodesChanged(TreeModelEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void treeNodesInserted(TreeModelEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void treeNodesRemoved(TreeModelEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void treeStructureChanged(TreeModelEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean evaluateBoolean(Object x) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object invoke(Context ctx, String method, Object arg) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAvailable(Context ctx) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled(Context ctx) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getUINodeType() {
		// TODO Auto-generated method stub
		return 0;
	}
	


}
