/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
/**
 * This package contains all classes that are required for the new point cloud
 * feature (the set of point cloud tools in the 3D menu). The class
 * PointCloudTools contains static methods that are also used in XL and in the
 * 3D Menu (the XML structure that is loaded via reflection).
 *
 * The classes PointCloud, Point and Octree are required for the clustering
 * algorithm. The algorithm could not use the other classes because the
 * functionality provided by the foreign classes did not fit to the the
 * algorithms implemented here.
 *
 * All point cloud functions are compatible to the point cloud in the class
 * de.grogra.imp3d.objects.PointCloud. The class PointCloudDialog contains
 * graphical dialogs for the interaction with the user. They are used when the
 * user clicks one of the options in the graphical point cloud menu.
 *
 * @author Maurice Mueller
 */
package de.grogra.pointcloud;