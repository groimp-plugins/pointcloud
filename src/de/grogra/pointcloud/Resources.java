/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
/**
 * This class provides the resources for this package. It is able to load
 * messages and default values of different data types. The language (or locale)
 * is automatically used from the operating system (the java default locale).
 *
 * @author Maurice Mueller
 */
public abstract class Resources {
	/**
	 * The resource bundle that is used to translate all texts used in the
	 * dialog boxes
	 */
	private static ResourceBundle bundle = null;
	/**
	 * Initializes this class and the resources bundle object if the object is
	 * null. If the object is already initialized, nothing happens.
	 */
	private static void initializeIfNecessary() {
		if (Resources.bundle != null) {
			return;
		}
		Locale locale = Locale.getDefault();
		ClassLoader loader = PointCloudDialog.class.getClassLoader();
		String name = "de.grogra.pointcloud.Resources";
		try {
			Resources.bundle = ResourceBundle.getBundle(name, locale, loader);
		} catch (MissingResourceException missingResourceException) {
			Resources.bundle = ResourceBundle.getBundle(name, Locale.ENGLISH, loader);
		}
	}
	/**
	 * Translates the key and returns the string that is contained in the
	 * resources files. If the text can not be found, the key is returned. The
	 * resource file is chosen in dependence of the default language. If the
	 * resource bundle is not initialized, it gets initialized.
	 *
	 * @param key The key of the text that should be translated
	 * @return The translated text or the key itself if it could not be found
	 */
	public static String translate(String key) {
		Resources.initializeIfNecessary();
		if (Resources.bundle.containsKey(key)) {
			return Resources.bundle.getString(key);
		} else {
			return key;
		}
	}
	/**
	 * Loads the string value with the given key from the resources file and
	 * returns the value. If the value is not defined, an exception is thrown.
	 *
	 * @param key The key that should be searched
	 * @return The string value that is defined with the given key in the
	 * resources file
	 * @throws MissingResourceException If the variable with the given key is
	 * not defined in the resources file
	 */
	public static String getString(String key) {
		Resources.initializeIfNecessary();
		if (Resources.bundle.containsKey(key)) {
			return Resources.bundle.getString(key);
		} else {
			String message = Resources.translate("cannotFindIntegerError");
			message += ": \"" + key + "\"";
			throw new MissingResourceException(message, Resources.class.getName(), key);
		}
	}
	/**
	 * Loads the boolean value with the given key from the resources file and
	 * returns the value. If the value is not defined or not castable to a
	 * boolean variable, an exception is thrown.
	 *
	 * @param key The key that should be searched
	 * @return The boolean value that is defined with the given key in the
	 * resources file
	 * @throws MissingResourceException If the variable with the given key is
	 * not defined in the resources file
	 * @throws IllegalArgumentException If the value in the resources file
	 * exists, but is not of boolean type
	 */
	public static boolean isBoolean(String key) {
		Resources.initializeIfNecessary();
		if (Resources.bundle.containsKey(key)) {
			String value = Resources.bundle.getString(key);
			
			if (value.toLowerCase().equals("true")) {
				return true;
			} else if (value.toLowerCase().equals("false")) {
				return false;
			} else {
				String message = Resources.translate("cannotCastBooleanError");
				message += ": " + key + " = \"" + value + "\"";
				throw new IllegalArgumentException(message);
			}
		} else {
			String message = Resources.translate("cannotFindBooleanError");
			message += ": \"" + key + "\"";
			throw new MissingResourceException(message, Resources.class.getName(), key);
		}
	}
	/**
	 * Loads the integer value with the given key from the resources file and
	 * returns the value. If the value is not defined or not castable to an
	 * integer variable, an exception is thrown.
	 *
	 * @param key The key that should be searched
	 * @return The integer number that is defined with the given key in the
	 * resources file
	 * @throws MissingResourceException If the variable with the given key is
	 * not defined in the resources file
	 * @throws IllegalArgumentException If the value in the resources file
	 * exists, but is not of integer type
	 */
	public static int getInt(String key) {
		Resources.initializeIfNecessary();
		if (Resources.bundle.containsKey(key)) {
			String value = Resources.bundle.getString(key);
			try {
				return Integer.parseInt(value);
			} catch (NumberFormatException numberFormatException) {
				String message = Resources.translate("cannotCastIntegerError");
				message += ": " + key + " = \"" + value + "\"";
				throw new IllegalArgumentException(message);
			}
		} else {
			String message = Resources.translate("cannotFindIntegerError");
			message += ": \"" + key + "\"";
			throw new MissingResourceException(message, Resources.class.getName(), key);
		}
	}
	/**
	 * Loads the float value with the given key from the resources file and
	 * returns the value. If the value is not defined or not castable to a float
	 * variable, an exception is thrown.
	 *
	 * @param key The key that should be searched
	 * @return The float number that is defined with the given key in the
	 * resources file
	 * @throws MissingResourceException If the variable with the given key is
	 * not defined in the resources file
	 * @throws IllegalArgumentException If the value in the resources file
	 * exists, but is not of float type
	 */
	public static float getFloat(String key) {
		Resources.initializeIfNecessary();
		if (Resources.bundle.containsKey(key)) {
			String value = Resources.bundle.getString(key);
			try {
				return Float.parseFloat(value);
			} catch (NumberFormatException numberFormatException) {
				String message = Resources.translate("cannotCastFloatError");
				message += ": " + key + " = \"" + value + "\"";
				throw new IllegalArgumentException(message);
			}
		} else {
			String message = Resources.translate("cannotFindFloatError");
			message += ": \"" + key + "\"";
			throw new MissingResourceException(message, Resources.class.getName(), key);
		}
	}
}