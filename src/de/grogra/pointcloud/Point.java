/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud;
/**
 * This class represents a point with three coordinates and a point cloud
 * reference. The point cloud id is used by the clustering algorithm and to be
 * able to import and export multiple point clouds from and to a single file. If
 * the point cloud reference is missing, all points are stored in one point
 * cloud.
 *
 * @author Maurice Mueller
 */
public class Point {
	/**
	 * The x coordinate of this point
	 */
	private double x;
	/**
	 * The y coordinate of this point
	 */
	private double y;
	/**
	 * The z coordinate of this point
	 */
	private double z;
	/**
	 * The point cloud reference of this point. This is required to cluster the
	 * points in a point cloud later.
	 */
	private PointCloud pointCloud;
	/**
	 * A constructor to get a point with three coordinates. The point cloud is
	 * set to null.
	 *
	 * @param x The x coordinate of the point
	 * @param y The y coordinate of the point
	 * @param z The z coordinate of the point
	 */
	public Point(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.pointCloud = null;
	}
	/**
	 * A constructor to clone a point. The point cloud reference is also copied.
	 *
	 * @param point The point to clone
	 */
	public Point(Point point) {
		this.x = point.getX();
		this.y = point.getY();
		this.z = point.getZ();
		this.pointCloud = point.getPointCloud();
	}
	/**
	 * Returns the distance between this point and the given point. The distance
	 * is calculated with the pythagorean theorem.
	 *
	 * @param point The other point
	 * @return The distance between this point and the given point
	 */
	public double getDistanceToPoint(Point point) {
		double x = point.getX() - this.getX();
		double y = point.getY() - this.getY();
		double z = point.getZ() - this.getZ();
		return Math.sqrt(x * x + y * y + z * z);
	}
	/**
	 * Returns the distance between this point and the given line (represented
	 * by a position vector and a direction vector).
	 *
	 * @param position The position vector of the line
	 * @param direction The direction vector of the line
	 * @return The distance between this point and that line
	 */
	public double getDistanceToLine(Vector position, Vector direction) {
		Vector vector = Vector.subtractVectors(this.toVector(), position);
		vector = Vector.getCrossProduct3(vector, direction);
		return vector.getPythagoreanLength() / direction.getPythagoreanLength();
	}
	/**
	 * Returns the distance between this point and the given plane (represented
	 * by a position vector and a normal vector).
	 *
	 * @param position The position vector of the plane
	 * @param normal The normal vector of the plane
	 * @return The distance between this point and that plane
	 */
	public double getDistanceToPlane(Vector position, Vector normal) {
		Vector vector = Vector.subtractVectors(this.toVector(), position);
		double scalar = Vector.getScalarProduct(vector, normal);
		return Math.abs(scalar) / normal.getPythagoreanLength();
	}
	/**
	 * Returns the signed distance between this point and the given plane
	 * (represented by a position vector and a normal vector). Signed means that
	 * the distance is negative if the point is below the plane and the distance
	 * is positive if the point is above the plane.
	 *
	 * @param position The position vector of the plane
	 * @param normal The normal vector of the plane
	 * @return The signed distance between this point and that plane
	 */
	public double getSignedDistanceToPlane(Vector position, Vector normal) {
		Vector vector = Vector.subtractVectors(this.toVector(), position);
		double scalar = Vector.getScalarProduct(vector, normal);
		return scalar / normal.getPythagoreanLength();
	}
	/**
	 * Sets the x coordinate of this point.
	 *
	 * @param x The new x coordinate for this point
	 */
	public void setX(double x) {
		this.x = x;
	}
	/**
	 * Returns the x coordinate of this point.
	 *
	 * @return The x coordinate of this point
	 */
	public double getX() {
		return this.x;
	}
	/**
	 * Sets the y coordinate of this point.
	 *
	 * @param y The new y coordinate for this point
	 */
	public void setY(double y) {
		this.y = y;
	}
	/**
	 * Returns the y coordinate of this point.
	 *
	 * @return The y coordinate of this point
	 */
	public double getY() {
		return this.y;
	}
	/**
	 * Sets the z coordinate of this point.
	 *
	 * @param z The new z coordinate for this point
	 */
	public void setZ(double z) {
		this.z = z;
	}
	/**
	 * Returns the z coordinate of this point.
	 *
	 * @return The z coordinate of this point
	 */
	public double getZ() {
		return this.z;
	}
	/**
	 * Sets the point cloud reference of this point.
	 *
	 * @param id The new point cloud reference for this point
	 */
	public void setPointCloud(PointCloud pointCloud) {
		this.pointCloud = pointCloud;
	}
	/**
	 * Returns the point cloud reference of this point.
	 *
	 * @return The point cloud reference of this point
	 */
	public PointCloud getPointCloud() {
		return this.pointCloud;
	}
	/**
	 * Converts this point to a vector and returns the vector. The point cloud
	 * information is not stored in the vector. The vector has always one
	 * dimension with three elements.
	 *
	 * @return The vector with the x, y, and z value of this point
	 */
	public Vector toVector() {
		return new Vector(new double[]{this.x, this.y, this.z});
	}
	/**
	 * Adds the position of the given other point to the position of this point.
	 *
	 * @param point An other point
	 */
	public void addCoordinates(Point point) {
		this.x += point.getX();
		this.y += point.getY();
		this.z += point.getZ();
	}
}