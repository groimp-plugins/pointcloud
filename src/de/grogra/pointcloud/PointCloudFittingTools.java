/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.Null;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.vecmath.Matrix4d;
/**
 * This class provides lots of methods to fit objects to point clouds.
 * Currently, spheres, cylinders, frustums and cones are supported. As an extra
 * feature, an automatic mode can be used. The automatic mode automatically
 * chooses the object with the best score. For all fitting methods, fibonacci
 * spheres are used. For the fibonacci spheres, the center of the concerning
 * point cloud is used as center of the sphere. The point with the largest
 * distance to the center is then used as radius. This class also provides lots
 * of helper methods that make the conversion between different data types
 * simpler.
 *
 * @author Maurice Mueller
 */
public abstract class PointCloudFittingTools {
	/**
	 * The minimum length of cylinders, frustums, and cones. This value is also
	 * the minimum diameter for spheres. This value is set as default size if
	 * fitted objects are too small or have a length of 0. If an object gets a
	 * length of 0, it is not displayed as circle, but not displayed instead.
	 * This is misleading behavior for the user and should be avoided. Because
	 * all fitting methods are only approximative algorithms, a minimal error
	 * for vanishing point clouds or objects should be justifiable.
	 */
	private static final double MINIMUM_OBJECT_LENGTH = Resources.getFloat("minimumObjectLength");
	/**
	 * Fits a sphere to the given point cloud, so that the sphere encloses all
	 * points of the point cloud and the volume is as minimal as possible. The
	 * average parameter can be set to true to fit the point cloud so that a
	 * regression is executed for all points and the average radius is used. If
	 * the average parameter is set to false, the maximum radius of all points
	 * is used.
	 *
	 * @param pointCloud The point cloud that should be used
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param The fitted sphere object
	 */
	public static Sphere fitSphereToPointCloud(PointCloud pointCloud, boolean average) {
		Point center = pointCloud.getCenter();
		double radius = 0.0;
		if (average) {
			double distanceSum = 0.0;
			List<Point> points = pointCloud.getPoints();
			int index = 0;
			while (index < points.size()) {
				distanceSum += points.get(index).getDistanceToPoint(center);
				index++;
			}
			radius = distanceSum / (double)(points.size());
		} else {
			Point point = pointCloud.getPointWithMostFarDistanceToCenterPosition();
			radius = center.getDistanceToPoint(point);
		}
		// The minimum length must be divided by 2 to get the radius instead of the diameter
		radius = Math.max(PointCloudFittingTools.MINIMUM_OBJECT_LENGTH / 2.0, radius);
		// rotation is null (because it's a sphere)
		return new Sphere(center.toVector(), null, radius);
	}
	/**
	 * Fits a cylinder to the given point cloud, so that the cylinder encloses
	 * all points of the point cloud and the volume is as minimal as possible.
	 * With the precision parameter, the number of points in the fibonacci
	 * sphere can be specified. The average parameter can be set to true to fit
	 * the point cloud so that a regression is executed for all points and the
	 * average radius is used. If the average parameter is set to false, the
	 * maximum radius of all points is used.
	 *
	 * @param pointCloud The point cloud that should be used
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param precision The number of points for the fibonacci sphere
	 * @param The fitted cylinder object
	 */
	public static Cylinder fitCylinderToPointCloud(PointCloud pointCloud, boolean average, int precision) {
		Cylinder cylinder = PointCloudFittingTools.fitCylinderWithFibonacciSphereAnalysis(pointCloud, precision);
		if (average) {
			List<Point> points = pointCloud.getPoints();
			Vector position = cylinder.getPosition();
			Vector direction = cylinder.getDirection();
			double distanceSum = 0.0;
			int index = 0;
			while (index < points.size()) {
				distanceSum += points.get(index).getDistanceToLine(position, direction);
				index++;
			}
			cylinder.setRadius(distanceSum / (double)(points.size()));
		}
		cylinder.setLength(Math.max(PointCloudFittingTools.MINIMUM_OBJECT_LENGTH, cylinder.getLength()));
		return cylinder;
	}
	/**
	 * Fits a frustum to the given point cloud, so that the frustum encloses all
	 * points of the point cloud and the volume is as minimal as possible. With
	 * the precision parameter, the number of points in the fibonacci sphere can
	 * be specified. The average parameter can be set to true to fit the point
	 * cloud so that a regression is executed for all points and the average
	 * radius is used. If the average parameter is set to false, the maximum
	 * radius of all points is used.
	 *
	 * @param pointCloud The point cloud that should be used
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param precision The number of points for the fibonacci sphere
	 * @param The fitted frustum object
	 */
	public static Frustum fitFrustumToPointCloud(PointCloud pointCloud, boolean average, int precision) {
		Cylinder cylinder = PointCloudFittingTools.fitCylinderToPointCloud(pointCloud, average, precision);
		double[] regression = PointCloudFittingTools.calculateLinearRegression(cylinder, pointCloud);
		double slope = regression[0];
		double intercept = regression[1];
		Vector position = cylinder.getPosition();
		Vector direction = cylinder.getDirection();
		// the baseRadius and topRadius are calculated for the case that all
		// points should be (in average) on the frustum surface
		double baseRadius = slope * 0.0 + intercept;
		double topRadius = slope * cylinder.getLength() + intercept;
		if (!average) {
			// If the maximum mode is active, the linear regression is shifted
			// away from the main axis. The regression is shifted so that the
			// point with the largest vertical distance to the regression makes
			// the new regression
			List<Point> points = pointCloud.getPoints();
			double maximumDistance = 0.0;
			int index = 0;
			while (index < points.size()) {
				Point point = points.get(index);
				double distanceToMainAxis = point.getDistanceToLine(position, direction);
				double distanceToBasePlane = point.getDistanceToPlane(position, direction);
				// The distanceToBasePlane is always between 0 and
				// cylinder.getLength() because the cylinder was created so that
				// it contains all points.
				double distance = distanceToMainAxis - (slope * distanceToBasePlane + intercept);
				if (distance > maximumDistance) {
					maximumDistance = distance;
				}
				index++;
			}
			baseRadius += maximumDistance;
			topRadius += maximumDistance;
		}
		double frustumLength = cylinder.getLength();
		frustumLength = Math.max(PointCloudFittingTools.MINIMUM_OBJECT_LENGTH, frustumLength);
		if (topRadius < baseRadius) {
			return new Frustum(position, direction, baseRadius, topRadius, frustumLength);
		} else {
			// flip frustum if top radius is larger than bottom radius
			Vector length = direction.clone();
			length.trimToLength(cylinder.getLength());
			Vector newPosition = Vector.addVectors(position, length);
			Vector newDirection = Vector.multiplyWithScalar(direction, -1);
			double newBaseRadius = topRadius;
			double newTopRadius = baseRadius;
			return new Frustum(newPosition, newDirection, newBaseRadius, newTopRadius, frustumLength);
		}
	}
	/**
	 * Fits a cone to the given point cloud, so that the cone encloses all
	 * points of the point cloud and the volume is as minimal as possible. With
	 * the precision parameter, the number of points in the fibonacci sphere can
	 * be specified. The average parameter can be set to true to fit the point
	 * cloud so that a regression is executed for all points and the average
	 * radius is used. If the average parameter is set to false, the maximum
	 * radius of all points is used.
	 *
	 * @param pointCloud The point cloud that should be used
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param precision The number of points for the fibonacci sphere
	 * @param The fitted cone object
	 */
	public static Cone fitConeToPointCloud(PointCloud pointCloud, boolean average, int precision) {
		Frustum frustum = PointCloudFittingTools.fitFrustumToPointCloud(pointCloud, average, precision);
		double ratio = frustum.getBaseRadius() / frustum.getTopRadius();
		double additionalLength = frustum.getLength() / (ratio - 1.0);
		double coneLength = additionalLength + frustum.getLength();
		coneLength = Math.max(PointCloudFittingTools.MINIMUM_OBJECT_LENGTH, coneLength);
		return new Cone(frustum.getPosition(), frustum.getDirection(), frustum.getBaseRadius(), coneLength);
	}
	public static MeshNode fitMeshToPointCloud(de.grogra.imp3d.objects.PointCloud pointCloud, boolean average, int precision) {
		return de.grogra.rgg.Library.triangulate(pointCloud.getPoints());
		
	}
	
	/**
	 * Fits an automatically selected object (sphere, cylinder, frustum or cone)
	 * to the given point cloud, so that the object encloses all points of the
	 * point cloud and the volume is as minimal as possible. With the precision
	 * parameter, the number of points in the fibonacci sphere can be specified.
	 * The average parameter can be set to true to fit the point cloud so that a
	 * regression is executed for all points and the average radius is used. If
	 * the average parameter is set to false, the maximum radius of all points
	 * is used.
	 *
	 * @param pointCloud The point cloud that should be used
	 * @param average Must be set to true to use average fitting and to false to
	 * use maximum fitting
	 * @param precision The number of points for the fibonacci sphere
	 * @param The fitted object
	 */
	public static Shape3D fitAutomaticObjectToPointCloud(PointCloud pointCloud, boolean average, int precision) {
		Sphere sphere = PointCloudFittingTools.fitSphereToPointCloud(pointCloud, average);
		Cylinder cylinder = PointCloudFittingTools.fitCylinderToPointCloud(pointCloud, average, precision);
		Frustum frustum = PointCloudFittingTools.fitFrustumToPointCloud(pointCloud, average, precision);
		Cone cone = PointCloudFittingTools.fitConeToPointCloud(pointCloud, average, precision);
		double sphereScore = sphere.getVolume();
		double cylinderScore = cylinder.getVolume();
		double frustumScore = frustum.getVolume();
		double coneScore = cone.getVolume();
		if (sphereScore < cylinderScore && sphereScore < frustumScore && sphereScore < coneScore) {
			return sphere;
		} else if (cylinderScore < frustumScore && cylinderScore < coneScore) {
			return cylinder;
		} else if (frustumScore < coneScore) {
			float maximum = Resources.getFloat("maximumTopRadiusForConeInsteadOfFrustum");
			float minimum = Resources.getFloat("minimumTopRadiusForCylinderInsteadOfFrustum");
			if (frustum.getTopRadius() < frustum.getBaseRadius() * maximum) {
				return cone;
			} else if (frustum.getTopRadius() > frustum.getBaseRadius() * minimum) {
				return cylinder;
			} else {
				return frustum;
			}
		} else {
			return cone;
		}
	}
	/**
	 * Converts an internal sphere to an XL compatible sphere and returns it.
	 * This conversion is necessary because the internal object is much easier
	 * to handle than the XL compatible one.
	 *
	 * @param sphere The sphere to convert
	 * @return The converted sphere
	 */
	public static de.grogra.imp3d.objects.Sphere convertSphere(Sphere sphere) {
		de.grogra.imp3d.objects.Sphere newSphere = new de.grogra.imp3d.objects.Sphere();
		PointCloudFittingTools.setRotationAndTranslation(newSphere, sphere.getPosition(), sphere.getDirection());
		newSphere.setRadius((float)(sphere.getRadius()));
		return newSphere;
	}
	/**
	 * Converts an internal cylinder to an XL compatible cylinder and returns
	 * it. This conversion is necessary because the internal object is much
	 * easier to handle than the XL compatible one.
	 *
	 * @param cylinder The cylinder to convert
	 * @return The converted cylinder
	 */
	public static de.grogra.imp3d.objects.Cylinder convertCylinder(Cylinder cylinder) {
		de.grogra.imp3d.objects.Cylinder newCylinder = new de.grogra.imp3d.objects.Cylinder();
		PointCloudFittingTools.setRotationAndTranslation(newCylinder, cylinder.getPosition(), cylinder.getDirection());
		newCylinder.setRadius((float)(cylinder.getRadius()));
		newCylinder.setLength((float)(cylinder.getLength()));
		return newCylinder;
	}
	/**
	 * Converts an internal frustum to an XL compatible frustum and returns it.
	 * This conversion is necessary because the internal object is much easier
	 * to handle than the XL compatible one.
	 *
	 * @param frustum The frustum to convert
	 * @return The converted frustum
	 */
	public static de.grogra.imp3d.objects.Frustum convertFrustum(Frustum frustum) {
		de.grogra.imp3d.objects.Frustum newFrustum = new de.grogra.imp3d.objects.Frustum();
		PointCloudFittingTools.setRotationAndTranslation(newFrustum, frustum.getPosition(), frustum.getDirection());
		newFrustum.setBaseRadius((float)(frustum.getBaseRadius()));
		newFrustum.setTopRadius((float)(frustum.getTopRadius()));
		newFrustum.setLength((float)(frustum.getLength()));
		return newFrustum;
	}
	/**
	 * Converts an internal cone to an XL compatible cone and returns it. This
	 * conversion is necessary because the internal object is much easier to
	 * handle than the XL compatible one.
	 *
	 * @param cone The cone to convert
	 * @return The converted cone
	 */
	public static de.grogra.imp3d.objects.Cone convertCone(Cone cone) {
		de.grogra.imp3d.objects.Cone newCone = new de.grogra.imp3d.objects.Cone();
		PointCloudFittingTools.setRotationAndTranslation(newCone, cone.getPosition(), cone.getDirection());
		newCone.setRadius((float)(cone.getBaseRadius()));
		newCone.setLength((float)(cone.getLength()));
		return newCone;
	}
	public static de.grogra.imp3d.objects.MeshNode convertMesh(MeshNode cone) {
		de.grogra.imp3d.objects.MeshNode newCone = new de.grogra.imp3d.objects.MeshNode();

		return newCone;
	}
	/**
	 * Converts an internal 3d shape to an XL compatible node and returns it.
	 * This conversion is necessary because the internal object is much easier
	 * to handle than the XL compatible one. This method decides on the type of
	 * the shape which node should be returned. The type of the returned node is
	 * always compatible to the type of the internal 3d shape. If the shape is
	 * null or of an unknown type, an exception is thrown with a descriptive
	 * error message.
	 *
	 * @param shape The 3d shape to convert
	 * @return The converted shape (as node object)
	 * @throws IllegalArgumentException If the shape has an unknown type
	 * @throws NullPointerException If the shape is null
	 */
	public static Node convertShape3D(Shape3D shape) throws IllegalArgumentException {
		if (shape == null) {
			String message = Resources.translate("nullShapeError");
			throw new NullPointerException(message);
		}
		if (shape instanceof Sphere) {
			return PointCloudFittingTools.convertSphere((Sphere)(shape));
		} else if (shape instanceof Cylinder) {
			return PointCloudFittingTools.convertCylinder((Cylinder)(shape));
		} else if (shape instanceof Frustum) {
			return PointCloudFittingTools.convertFrustum((Frustum)(shape));
		} else if (shape instanceof Cone) {
			return PointCloudFittingTools.convertCone((Cone)(shape));
		} else {
			String message = Resources.translate("unknownShapeError");
			throw new IllegalArgumentException(message);
		}
	}
	/**
	 * Fits a cylinder to the given point cloud and returns it. With the
	 * precision parameter, the number of virtual test points can be specified.
	 * As higher the number of points, as higher the calculation time and as
	 * higher the precision. Recommended value is 1000. If the point cloud is
	 * null or empty, an exception is thrown.
	 *
	 * @param pointCloud The point cloud to fit the cylinder to
	 * @param precision The number of points for the fitting algorithm
	 * @return The fitting cylinder with all parameters set
	 * @throws IllegalArgumentException If the point cloud is empty
	 * @throws NullPointerException If the point cloud is null
	 */
	private static Cylinder fitCylinderWithFibonacciSphereAnalysis(PointCloud pointCloud, int precision) {
		if (pointCloud == null) {
			String message = Resources.translate("nullPointCloudError");
			throw new NullPointerException(message);
		}
		int number = pointCloud.getNumberOfPoints();
		if (number == 0) {
			String message = Resources.translate("emptyPointCloudError");
			throw new IllegalArgumentException(message);
		}
		Point center = pointCloud.getCenter();
		Point mostFarPoint = pointCloud.getPointWithMostFarDistanceToCenterPosition();
		double radius = mostFarPoint.getDistanceToPoint(center);
		int numberOfPoints = precision;
		List<Point> sphere = PointCloudFittingTools.createFibonacciSphere(center, radius, numberOfPoints);
		double score = Double.MAX_VALUE;
		Cylinder bestCylinder = null;
		int index = 0;
		while (index < numberOfPoints) {
			Vector direction = Vector.subtractVectors(sphere.get(index).toVector(), center.toVector());
			Cylinder cylinder = PointCloudFittingTools.fitCylinderToPointCloudWithGivenDirection(pointCloud, direction);
			//double currentScore = cylinder.getScore(pointCloud);
			// This is possible because the cylinder is always the
			// bounding-cylinder for the point cloud and the cylinder with the
			// smallest volume is the best one.
			double currentScore = cylinder.getVolume();
			if (currentScore < score) {
				score = currentScore;
				bestCylinder = cylinder;
			}
			index++;
		}
		return bestCylinder;
	}
	/**
	 * Creates and returns a cylinder that fits to the given point cloud and the
	 * given direction. The returned cylinder has its center point in the center
	 * point of the point cloud and the direction vector is the one that was
	 * given. The length and the radius of the cylinder are adapted, so that all
	 * points of the point cloud are contained in the cylinder, but the volume
	 * of the cylinder is as small as possible.
	 *
	 * @param pointCloud The point cloud that should be used for the fitting
	 * @param direction The direction vector that should be used for the fitting
	 * @return The cylinder with the fitting center point, radius and length
	 */
	private static Cylinder fitCylinderToPointCloudWithGivenDirection(PointCloud pointCloud, Vector direction) {
		Point center = pointCloud.getCenter();
		double maximumLength = 0;// distance to plane on center point and direction vector as normal vector
		double maximumRadius = 0;// distance to normal vector (direction vector)
		int index = 0;
		while (index < pointCloud.getPoints().size()) {
			Point point = pointCloud.getPoints().get(index);
			double distanceToPlane = point.getDistanceToPlane(center.toVector(), direction);
			double distanceToNormal = point.getDistanceToLine(center.toVector(), direction);
			if (distanceToPlane > maximumLength) {
				maximumLength = distanceToPlane;
			}
			if (distanceToNormal > maximumRadius) {
				maximumRadius = distanceToNormal;
			}
			index++;
		}
		Vector length = direction.clone();
		length.trimToLength(maximumLength);
		return new Cylinder(Vector.subtractVectors(center.toVector(), length), direction, maximumRadius, 2 * maximumLength);
	}
	/**
	 * Creates and returns a fibonacci sphere, represented by a point cloud that
	 * is compatible to XL scripts. With the parameters, the center position,
	 * the radius and the number of points can be specified.
	 *
	 * @param x The x value of the center position for the fibonacci sphere
	 * @param y The y value of the center position for the fibonacci sphere
	 * @param z The z value of the center position for the fibonacci sphere
	 * @param radius The radius for the fibonacci sphere
	 * @param number The number of points for the fibonacci sphere
	 * @return The list of points that represent the fibonacci sphere
	 */
	public static de.grogra.imp3d.objects.PointCloud getFibonacciSphere(double x, double y, double z, double radius, int number) {
		Point point = new Point(x, y, z);
		List<Point> points = PointCloudFittingTools.createFibonacciSphere(point, radius, number);
		PointCloud pointCloud = new PointCloud(points);
		pointCloud.setColor(PointCloud.getDefaultPointCloudColor());
		return PointCloudTools.convertPointCloud(pointCloud);
	}
	/**
	 * Creates and returns a fibonacci sphere, represented by a list of points.
	 * With the parameters, the center position, the radius and the number of
	 * points can be specified.
	 *
	 * @param center The center position for the fibonacci sphere
	 * @param radius The radius for the fibonacci sphere
	 * @param number The number of points for the fibonacci sphere
	 * @return The list of points that represend the fibonacci sphere
	 */
	private static List<Point> createFibonacciSphere(Point center, double radius, int number) {
		List<Point> points = new ArrayList<>();
		double phi = Math.PI * (3.0 - Math.sqrt(5));
		int index = 0;
		while (index < number) {
			double y = radius - ((double)(index) / (double)(number - 1)) * 2.0 * radius;
			double internalRadius = Math.sqrt(radius * radius - y * y);
			double theta = phi * (double)(index);
			double x = Math.cos(theta) * internalRadius;
			double z = Math.sin(theta) * internalRadius;
			points.add(new Point(center.getX() + x, center.getY() + y, center.getZ() + z));
			index++;
		}
		return points;
	}
	/**
	 * Creates an example point cloud with a cylindric form, a shear, a rotation
	 * and a translation. The returned point cloud is compatible to XL scripts,
	 * has a default color and can be used as example point cloud for the
	 * fitting algorithm. The parameters "center", "radius" and "length" are the
	 * most important parameters for the example cylinder. With "number", the
	 * number of points can be specified. With the parameter "noise", the noise
	 * of each point can be set.
	 *
	 * @param number The number of points in the example point cloud
	 * @param center The center position of the cylinder
	 * @param length The length of the cylinder
	 * @param radius The radius of the cylinder
	 * @param noise The noise of the cylinder
	 * @return The list of points that describe the generated point cloud
	 */
	public static de.grogra.imp3d.objects.PointCloud getCylindricPointCloud(int number, Point center, double length, double radius, double noise) {
		List<Point> points = PointCloudFittingTools.createCylindricPointCloud(number, center, length, radius, noise);
		PointCloud pointCloud = new PointCloud(points);
		pointCloud.setColor(PointCloud.getDefaultPointCloudColor());
		return PointCloudTools.convertPointCloud(pointCloud);
	}
	/**
	 * Creates an example point cloud with a cylindric form, a shear, a rotation
	 * and a translation. The returned list of points can be used as example
	 * point cloud for the fitting algorithm. The parameters "center", "radius"
	 * and "length" are the most important parameters for the example cylinder.
	 * With "number", the number of points can be specified. With the parameter
	 * "noise", the noise of each point can be set.
	 *
	 * @param number The number of points in the example point cloud
	 * @param center The center position of the cylinder
	 * @param length The length of the cylinder
	 * @param radius The radius of the cylinder
	 * @param noise The noise of the cylinder
	 * @return The list of points that describe the generated point cloud
	 */
	private static List<Point> createCylindricPointCloud(int number, Point center, double length, double radius, double noise) {
		List<Point> points = new ArrayList<>();
		int layers = number / 8;
		double step = (double)(length) / (double)(layers);
		double diagonal = Math.sqrt(2);
		double slope = 1.5;
		double y = 0;
		while (y <= length) {
			points.add(new Point(radius, y, radius + y * slope));
			points.add(new Point(0, y, radius * diagonal + y * slope));
			points.add(new Point(-radius, y, radius + y * slope));
			points.add(new Point(radius * -diagonal, y, 0 + y * slope));
			points.add(new Point(-radius, y, -radius + y * slope));
			points.add(new Point(0, y, radius * -diagonal + y * slope));
			points.add(new Point(radius, y, -radius + y * slope));
			points.add(new Point(radius * diagonal, y, 0 + y * slope));
			y += step;
		}
		Random random = new Random(0);
		int index = 0;
		while (index < points.size()) {
			Point point = points.get(index);
			double randomX = (Math.abs((double)(random.nextInt()) / (double)(random.nextInt())) % 1.0) * 2.0 * noise - noise;
			double randomY = (Math.abs((double)(random.nextInt()) / (double)(random.nextInt())) % 1.0) * 2.0 * noise - noise;
			double randomZ = (Math.abs((double)(random.nextInt()) / (double)(random.nextInt())) % 1.0) * 2.0 * noise - noise;
			Point noisePoint = new Point(randomX, randomY, randomZ);
			point.addCoordinates(noisePoint);
			point.addCoordinates(center);
			index++;
		}
		return points;
	}
	/**
	 * Sets the rotation and the translation of an object (extending Null). This
	 * can be used for nearly all objects from the de.grogra.imp3d.objects
	 * package. All other values in the translation matrix are reset by
	 * executing this method.
	 *
	 * @param object The object to set the position and rotation
	 * @param position The new position vector for the object
	 * @param direction The new direction vector for the object
	 */
	public static void setRotationAndTranslation(Null object, Vector position, Vector direction) {
		if (direction != null) {
			double x = direction.get(0);
			double y = direction.get(1);
			double z = direction.get(2);
			double radius = Math.sqrt(x * x + y * y + z * z);
			double xAngle = 0;
			double yAngle = Math.acos(z / radius);
			double zAngle = Math.atan2(y, x);
			object.setRotation(xAngle, yAngle, zAngle);
		}
		Matrix4d result = object.getLocalTransformation();
		result.m03 = position.get(0);
		result.m13 = position.get(1);
		result.m23 = position.get(2);
		object.setTransform(result);
	}
	/**
	 * Calculates the linear regression for all points in the point cloud. The
	 * linear regression in the returned value is the average circular surface
	 * for the frustum (the x axis is the direction vector of the cylinder or
	 * frustum and the y axis is the radius). The returned double array consists
	 * of two elements and contains the slope in the first element and the
	 * intercept in the second element.
	 *
	 * @param cylinder The cylinder to use as reference position and direction
	 * for the points
	 * @param pointCloud The point clouds to use the points from
	 * @return An array with two elements (the slope and the intercept)
	 * @throws IllegalArgumentException If the input arrays are invalid
	 */
	private static double[] calculateLinearRegression(Cylinder cylinder, PointCloud pointCloud) {
		Vector position = cylinder.getPosition();
		Vector direction = cylinder.getDirection();
		List<Point> points = pointCloud.getPoints();
		double[] positions = new double[points.size()];
		double[] values = new double[points.size()];
		int index = 0;
		while (index < points.size()) {
			Point point = points.get(index);
			positions[index] = point.getDistanceToPlane(position, direction);
			values[index] = point.getDistanceToLine(position, direction);
			index++;
		}
		return PointCloudFittingTools.calculateLinearRegression(positions, values);
	}
	/**
	 * Calculates the linear regression of the given data points and returns an
	 * array with exactly two double values. The first value is the slope of the
	 * function and the second value is the intercept of the function. If the
	 * input arrays are null, too short, or do not have the same length, an
	 * exception is thrown. The arrays must have a minimum length of two
	 * elements, otherwise the slope of the regression can not be calculated.
	 *
	 * @param positions The positions of the data points in the first dimension
	 * @param values The values of the data points in the second dimension
	 * @return An array with two elements (the slope and the intercept)
	 * @throws IllegalArgumentException If the input arrays have an unequal
	 * length or have less than two elements
	 * @throws NullPointerException If one of the arrays or both arrays are null
	 */
	public static double[] calculateLinearRegression(double[] positions, double[] values) {
		if (positions == null || values == null) {
			String message = Resources.translate("nullArraysError");
			throw new NullPointerException(message);
		}
		if (positions.length != values.length) {
			String message = Resources.translate("differentArrayLengthsError");
			throw new IllegalArgumentException(message);
		}
		if (positions.length < 2 || values.length < 2) {
			String message = Resources.translate("arraysTooShortError");
			throw new IllegalArgumentException(message);
		}
		double sumX = 0.0;
		double sumY = 0.0;
		int index = 0;
		while (index < positions.length) {
			sumX += positions[index];
			sumY += values[index];
			index++;
		}
		double averageX = sumX / (double)(positions.length);
		double averageY = sumY / (double)(values.length);
		sumX = 0.0;
		sumY = 0.0;
		index = 0;
		while (index < positions.length) {
			double difference = positions[index] - averageX;
			sumX += difference * (positions[index] - averageX);
			sumY += difference * (values[index] - averageY);
			index++;
		}
		double slope = sumY / sumX;
		double intercept = averageY - slope * averageX;
		return new double[]{slope, intercept};
	}
}