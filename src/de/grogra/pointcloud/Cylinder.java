/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package de.grogra.pointcloud;
/**
 * This class represents a cylinder. This class is used for the point cloud
 * fitting feature because it is much more simple to use than the original
 * class.
 *
 * @author Maurice Mueller
 */
public class Cylinder extends Shape3D {
	/**
	 * The radius of this cylinder
	 */
	private double radius;
	/**
	 * The length of this cylinder
	 */
	private double length;
	/**
	 * Creates a cylinder object with a position, a direction, a radius and a
	 * length.
	 *
	 * @param position The position for this cylinder
	 * @param direction The direction for this cylinder
	 * @param radius The radius for this cylinder
	 * @param length The length for this cylinder
	 */
	public Cylinder(Vector position, Vector direction, double radius, double length) {
		super(position, direction);
		this.radius = radius;
		this.length = length;
	}
	/**
	 * Sets the radius of this cylinder.
	 *
	 * @param radius The new radius for this cylinder
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}
	/**
	 * Returns the radius of this cylinder.
	 *
	 * @return The radius of this cylinder
	 */
	public double getRadius() {
		return this.radius;
	}
	/**
	 * Sets the length of this cylinder.
	 *
	 * @param length The new length for this cylinder
	 */
	public void setLength(double length) {
		this.length = length;
	}
	/**
	 * Returns the length of this cylinder.
	 *
	 * @return The length of this cylinder
	 */
	public double getLength() {
		return this.length;
	}
	/**
	 * Calculates and returns the minimum distance of the given point to the
	 * surface of this cylinder.
	 *
	 * WARNING: The distance of points above and below the cylinder may be
	 * wrong. In these cases, only the distance to the bottom surface or to the
	 * top surface is returned!
	 *
	 * @param point The point to get the minimum distance to the surface of this
	 * cylinder for
	 * @return The minimum distance of the given point to the surface of this
	 * cylinder
	 */
	public double getMinimumDistanceToSurface(Point point) {
		// The distance must be calculated for the base, for the top and for the
		// circular surface individually. Then, the minimum distance is returned
		Vector normal = this.getDirection().clone();
		normal.trimToLength(this.length);
		Vector bottomPosition = this.getPosition();
		Vector topPosition = Vector.addVectors(bottomPosition, normal);
		double distanceBase = point.getSignedDistanceToPlane(bottomPosition, normal);
		double distanceTop = point.getSignedDistanceToPlane(topPosition, normal);
		// If the distance to the base plane is negative, the point is below the
		// cylinder. The distance to the cylindrical surface is then ignored and
		// the positive distance to the base plane is returned.
		if (distanceBase < 0) {
			return -distanceBase;
		}
		// If the distance to the top plane is positive, the point is above the
		// cylinder. The distance to the cylindrical surface is then ignored and
		// the distance to the top plane is returned.
		if (distanceTop > 0) {
			return distanceTop;
		}
		distanceTop = -distanceTop;
		// Here, the point is in the range between the base position and the top
		// position
		double distanceSide = point.getDistanceToLine(bottomPosition, normal);
		distanceSide = Math.abs(this.radius - distanceSide);
		if (distanceBase < distanceSide && distanceBase < distanceTop) {
			return distanceBase;
		} else if (distanceSide < distanceBase && distanceSide < distanceTop) {
			return distanceSide;
		} else {
			return distanceTop;
		}
	}
	/**
	 * Calculates and returns the volume of this cylinder.
	 *
	 * @return The volume of this cylinder
	 */
	public double getVolume() {
		return Math.PI * this.radius * this.radius * this.length;
	}
	/**
	 * Calculates and returns the surface of this cylinder.
	 *
	 * @return The surface of this cylinder
	 */
	public double getSurface() {
		double base = Math.PI * this.radius * this.radius;
		double side = 2.0 * this.radius * Math.PI * this.length;
		return 2.0 * base + side;
	}
}